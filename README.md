## 平台简介

https://gitee.com/whiteshader/ruoyi-cloud-vben

相关技术：

* 前端采用Vue 3.0、Ant Design Vue 3、TypeScript。
* 后端采用Spring Boot、Spring Security、Redis & Jwt。
* 权限认证使用Jwt，支持多终端认证系统。
* 支持加载动态权限菜单，多方式轻松权限控制。
* 高效率开发，使用代码生成器可以一键生成前后端代码。


## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 缓存监控：对系统的缓存信息查询，命令统计等。
17. 在线构建器：拖动表单元素生成相应的HTML代码。
18. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## 演示方法：

- 注意需要联网
- 解压nginx.zip
- 双击nginx.exe
- 浏览器打开http://localhost

## 帐号：

- admin/admin123  
- ry/admin123

## 前端开发注意事项

- Node：建议v16或以上

- 包管理：pnpm (安装方式：npm i -g pnpm)

- 安装依赖请支行：pnpm i

- 正常启动请运行: pnpm run serve

- 发布打包请运行: npm run build

## 相关技术文档

### TypeScript
https://www.tslang.cn/docs/home.html

### Vue Js
https://cn.vuejs.org/guide/quick-start.html

### Ant Design Vue
https://antdv.com/components/overview

### Vben Admin
http://doc.vvbin.cn/guide/introduction.html


## 部署
http://doc.ruoyi.vip/ruoyi-vue/document/hjbs.html#nginx%E9%85%8D%E7%BD%AE

## 演示图


<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-330d62945f979335cd09f040269994e365c.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-bb8692fbfbb513c5ccf93bb973cefbc286f.png"/></td>
    </tr>    
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-66299b907b89a1f623d90e695164e296c71.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-19ede2dcb3a69c751425f06c4d78afd22a1.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-5b0e51b199b979ef653dccf6f678811d0f0.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-a0f768d23b612aec54689d8655ad87bd37a.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-aa0f92ebeac98a177983b1dafeb4988ea81.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-a90e4959872212633a0877c8035d50214f7.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-95afadfe8dcc20c4c77c8fca640bd07a5b0.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-a1bf6d09cb1d1b7527c91ccb235be9e73a1.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d129e3a9fe98c5b33024f1af51fff5bc38a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-99aa6732bb023d0a8bbcfe7b712385636e2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-20a144288d6a8c3840e6a1b650b2fd61afd.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-4133996ffdc6dfe3bb40e294fb2cf613f36.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-4f11943333a36fe30e6cdedff72281887a1.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c44f4c73c2925352e01de36b6db8da9f8ae.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c961717c0b5d1e73730411f12554ff3390a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-e8cbdbd957cdc314ea9217ff1c4f9cf0428.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-2022a1412a28f08f5f0a60e76bb6ce27b79.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-144f59be1e40a8ea6c976b9445030cd5a0e.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d32c2a1c6d07d7869172f04a1974dd8cc95.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-53368ca9c24bb1d46621a3af2db47591b40.png"/></td>
    </tr>
</table>


## 前后端分离交流群

QQ群： [![加入QQ群](https://img.shields.io/badge/201396349-blue.svg)](https://jq.qq.com/?_wv=1027&k=u58VEEQK) 点击按钮入群。
